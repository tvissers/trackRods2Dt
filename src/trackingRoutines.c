/*
    This file is part of trackRods2Dt.

    trackRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    trackRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with trackRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "types.h"

bool WARNINGS;
double TT;

int NUMBEROFCONNECTIONS;
int EXCEPTIONONTHELIST;
int EXCEPTIONATTEMPTS;

bool compareWithOthers(int m, int i, int** nearestNeighbourTable_z, int** connectivityTable, int* n_part_z, int z, tBac ** particles, int endList, int depth, int recDepth);

/* Function: setLastMovement
	Set last measured displacement in x-direction and y-direction

        Parameters:
                tBac1 - bacteria one
                tBac2 - bacteria two
*/
void setLastMovement(tBac *v1, tBac *v2)
{
	v2->lastMovement.x = v2->x-v1->x;
	v2->lastMovement.y = v2->y-v1->y;
}

/* Function: distance2
	Calculates the squared distance between two bacteria, subtracting the displacements between the previous two frames.

        Parameters:
                tBac1 - bacteria one
                tBac2 - bacteria two

        Returns:
		Squared distance between the two bacteria positions
*/
double distance2(tBac v1, tBac v2)
{
	return ( (v2.x-v1.x-v1.lastMovement.x)*(v2.x-v1.x-v1.lastMovement.x) + (v2.y-v1.y-v1.lastMovement.y)*(v2.y-v1.y-v1.lastMovement.y) );
}

/* Function: isSelected

        Parameters:
		m - particle index
		connectivityTable_z - particles in trajectories at frame z
		max_n - number of trajectories found until now

	Returns:
		true or false - true: if particle m was already present at level z in the connectivityTable, false: if it wasn't
*/
bool isSelected(int m, int* connectivityTable_z, int max_n)
{	
	int n;
	for (n =0; n < max_n; n++)
	{
		if (connectivityTable_z[n] == m)
			return true;
	}
	return false;
}

/* Function: updateIndices

        Parameters:
                connectivityTable_z - particles in trajectories at frame z
                max_n - number of trajectories found until now
                n_z - number of particles in frame z
                lastIndices - list of last indices 
*/
void updateIndices(int* connectivityTable_z, int max_n, int n_z, int* lastIndices)
{
        int index;
	int m;
        for (index = 0; index < max_n; index++) // look up particle m in frame z
        {
		m = connectivityTable_z[index]; // particle number in frame z
		if (m < n_z)
			lastIndices[m]=index;
        }
}

/* Function: findBestCandidate
	Recursive function that attempts to identify the best candidate using a sorted list of nearest neighbours

        Parameters:
		m - this particle
		nearestNeighbourTable_z - list of nearest neighbours for each particle in fram z
                connectivityTable - contains connections between particles in consecutive frames
                n_part_z - list of the number of particles for each frame
                z - frame number
                particles - contains a list of particle coordinates for each frame
                endList - number of trajectories found until now
                depth - number of frames
                recDepth - recursion depth of looking for candidates

	Returns:
		cand - best candidate
*/
int findBestCandidate(int m, int** nearestNeighbourTable_z, int** connectivityTable, int* n_part_z, int z, tBac ** particles, int endList, int depth, int recDepth)
{
        int i;
        recDepth++;
        int cand = 100000000;
        bool continueSearch = false;
        bool found_candidate = false;

        if ( !(recDepth > 5) ) // if recDepth > 100, we're allmost certainly in a infinite recursion loop! (it is worthwhile to check for something better)
        {
                for (i =1; ( (i < (nearestNeighbourTable_z[m][0]+1)) && (!found_candidate) ); i++) // check all candidates until you find one.  
                {
                        if (!(isSelected(nearestNeighbourTable_z[m][i], connectivityTable[z+1], endList) ) )
                        {
                                continueSearch = compareWithOthers(m, i, nearestNeighbourTable_z, connectivityTable, n_part_z, z, particles, endList, depth, recDepth);
                                if (!continueSearch) // There was no better candidate, so the candidate was fine!
                                {
                                        found_candidate = true;
                                        cand = nearestNeighbourTable_z[m][i];
                                }
                        }
                }
        }
        else if (recDepth > 10 && WARNINGS)
	{
                printf("Warning: recursion depth of > 10 reached, very likely to be infinite loop. The recursion lookup for this particle candidate was stopped. \n");
	}
        return cand;
}

/* Function: compareWithOthers
        Compares this candidate with the other candidates in the nearest neighbour list

        Parameters:
                m - this particle
                i - index of candidate in the nearest neighbour list
                nearestNeighbourTable_z - list of nearest neighbours for each particle in fram z
                connectivityTable - contains connections between particles in consecutive frames
                n_part_z - list of the number of particles for each frame
                z - frame number
                particles - contains a list of particle coordinates for each frame
                endList - number of trajectories found until now
                depth - number of frames
                recDepth - recursion depth of looking for candidates

        Returns:
                true or false - true: if it found a better candidate, false: if not
*/
bool compareWithOthers(int m, int i, int** nearestNeighbourTable_z, int** connectivityTable, int* n_part_z, int z, tBac ** particles, int endList, int depth, int recDepth)
{
	int n;
	int j;
	bool foundBetterCandidate = false;
	if (m < n_part_z[z]) 
	{
		for (n = 1; (n < n_part_z[z] && !foundBetterCandidate); n++) // check if the candidate i was already a better candidate for particle n.
		{
			if (m != n)
			{
				for (j =1; (j < (nearestNeighbourTable_z[n][0]+1) && !foundBetterCandidate); j++) // check if there are more particles that have this candidate as a priority
				{
					//if (j <= i) // if the priority for this candidate is higher or equal for particle n.
					//{
						if ( (nearestNeighbourTable_z[n][j] == nearestNeighbourTable_z[m][i]) && ( distance2(particles[z][n], particles[z+1][nearestNeighbourTable_z[n][j]]) < distance2(particles[z][m], particles[z+1][nearestNeighbourTable_z[m][i]]) ) ) 
						{
							// n is closer than m, now we are going to find out if the candidate that we first found for particle m, 
							// is the best candidate for n. If it is, it can no longer be a candidate for m.
							if (recDepth == 1)
								EXCEPTIONATTEMPTS++;
							int L = findBestCandidate(n, nearestNeighbourTable_z, connectivityTable, n_part_z, z, particles, endList, depth, recDepth);
							if (L == nearestNeighbourTable_z[n][j] )
							{
								// foundBetterCandidate should be only true if both: 
								// 1. distance2(m, candidate i) > distance2(n, candidate i)
								// 2. candidate i is really the best candidate for particle n. 
								foundBetterCandidate = true;				
								if (recDepth == 1)
									EXCEPTIONONTHELIST++;
							}
						}
					//}
				}
			}
		}	
	}
	return foundBetterCandidate;
}

/* Function: linkList
	Links a particle in frame z to a particle in frame z+1 using the connectivityTable and by finding the best candidate

        Parameters:
                nearestNeighbourTable - list of nearest neighbours for each particle in each frame
		connectivityTable - contains connections between particles in consecutive frames
                n_part_z - list of the number of particles for each frame
                z - frame number
                particles - contains a list of particle coordinates for each frame
                endList - number of trajectories found until now
                depth - number of frames
		lastIndices - list of last indices

	Returns:
		endList - updated number of trajectories found until now
*/
int linkList(int** nearestNeighbourTable_z, int** connectivityTable, int* n_part_z, int z, tBac ** particles, int endList, int depth, int* lastIndices)
{
	int m;
		
	// put NEW particles in connectivity table, layer z:
	for (m = 0; m < n_part_z[z]; m++) // for particle m	
	{
		if (!isSelected(m, connectivityTable[z], endList) ) // if the particle is not connected to previous frame it is not yet in the connectivity table. Hence, this particle must be new.
		{
			connectivityTable[z][endList] = m; // make a new trajectory, starting at frame z
			endList++;
		}
	}

	updateIndices(connectivityTable[z], endList, n_part_z[z], lastIndices);
	
	int index;
	// links particles on level z to particles in level z+1 
	// on the basis of a SORTED list of candidates for linkage.

	// == candidate on two conditions: 1) candidate is not yet selected 
	// 2) there is no other particle m+n that has a higher priority for this candidate.

	EXCEPTIONONTHELIST = 0;
	for (m = 0; m < n_part_z[z]; m++) // for particle m
	{
		int recDepth = 0;
		int k = findBestCandidate(m, nearestNeighbourTable_z, connectivityTable, n_part_z, z, particles, endList, depth, recDepth); // calling recursive function!
		
		if (k < 100000000) // a candidate was found
		{	
			//printf("found\n");	
			NUMBEROFCONNECTIONS++;
			index = lastIndices[m];

			if (k < 0 || (k > n_part_z[z+1]) ) 
				printf("Error! %i",nearestNeighbourTable_z[m][0]+1);
			
			connectivityTable[z+1][index] = k;					// connects a particle m in layer z with a particle k in layer z+1
	
			setLastMovement(&particles[z][m], &particles[z+1][k]);
		}	
	}		

	return endList;
}

/* Function: updateCandidates
        Updates the list of candidates

        Parameters:
		candidates - list of candidates
                particles - contains a list of particle coordinates for each frame
		z - current frame
		m - this particle 
		n - new candidate

	Returns:
		candidates -updated list of candidates
*/
int* updateCandidates(int* candidates, tBac ** particles, int z, int m, int n)
{
	int i=1;
	int j;
	int insert_i = 0;

	// In this routine the list of candidates is sorted in descending order of likeliness

	if (candidates[0] == 0)
	{
		candidates[0]++;
		candidates = (int*) realloc(candidates, (candidates[0]+1)*sizeof(int));
		candidates[1] = n;
	}
	else if (candidates[0] > 0) // if there are already other candidates in the list
	{
		bool added = 0;
		
		while (i < (candidates[0]+1)) // as long as i is smaller than the total of candidates already in the list
		{
			if ( distance2(particles[z][m], particles[z+1][candidates[i]] ) > distance2(particles[z][m], particles[z+1][n]) && (!added) ) // if result between m and candidate i is larger than result between m and n
			{
				insert_i = i;
				added = 1;
			}
			i++;
		}
		
		if (insert_i == 0) // no candidates are found with larger result --> {particles[z][m], particles[z][n]} has largest result --> hence needs to be at the end of the array.
		{
			insert_i = candidates[0]+1;	
		}
		
		int* temp = (int*) malloc( (candidates[0]+1)* sizeof(int)); // temp candidates
		
		for (j = 0; j< candidates[0]+1; j++)
			temp[j] = candidates[j];				

		// reallocate
		candidates = (int*) realloc(candidates, (candidates[0]+2)*sizeof(int)); // +2 == 1 more than it was (remember first entry is for size).
		
		// shift all positions +1, starting from insert_i.
		for (i = insert_i; i < (candidates[0]+1); i++)
		{
			candidates[i+1] = temp[i]; // move all one to the right, starting with insert_i and eding with last index.
		}
		candidates[0]++; // the number of candidates in the array is increased with one
		candidates[insert_i] = n; // insert the new candidate in the array
		
		free(temp);
	}
	
	// error check
	for (j = 1; j< candidates[0]+1;j++)
	{
		if (candidates[j] < 0)
		{
			// this should never happen.
			printf("ERROR!!!!!!!!!\n");
			exit(666);	
		}
	}

	return candidates;
}

/* Function: writeTracksToFiles
        Writing trajectories to an output file

        Parameters:
		outputTracks - output file for trajectories
                connectivityTable - contains connections between particles in consecutive frames
                n_part_z - list of the number of particles for each frame
                particles - contains a list of particle coordinates for each frame
                startFrame - earliest frame where a trajectory can start
		endList - total number of found trajectories
                depth - number of frames
                version - version of the file format
*/
void writeTracksToFiles(char* outputTracks, int** connectivityTable, int* n_part_z, tBac ** particles, int startFrame, int endList, int depth, double version)
{
        printf("*** Writing valid trajectories to a file..\n");
	FILE* fp = fopen(outputTracks,"w");

        int minTrackLength = 2;
        int validTracks = 0;

	int n;
  	int z;
        for(n = 0; n < endList; n++)
        {
                int trackLength=0;
                for (z=0; z < depth; z++)
                {
                        int p = connectivityTable[z][n];
                        if (p < n_part_z[z]) // if it is an existing particle
                        {
                                trackLength++;
                        }
                }
                if (trackLength >= minTrackLength)
                        validTracks++;
        }

        printf("--> Found %i/%i tracks with length greater than or equal to %i frames.\n", validTracks, endList, minTrackLength);
       
	// the following lines have to do with backward compatibility issues:
	if (version > 0.0)
                fprintf(fp, "&%i &%i %f\n", validTracks, depth, 1.0);
        else if (version < 0.0)
                fprintf(fp, "&%i &%i %f\n", validTracks, depth, version);
        else
	{
                printf("Error, undefined file format version!\n");
		exit(666);
	}
	// writing the actual data:
        int trackNo=0;
        for(n = 0; n < endList; n++) // iterating over all found trajectories
        {
                int trackLength=0;
                for (z=0; z < depth; z++)
                {
                        int p = connectivityTable[z][n];
                        if (p < n_part_z[z]) // if it is an existing particle
                        {
                                trackLength++;
                        }
                }

                if (trackLength >= minTrackLength)
                {

                        fprintf(fp, "&%i &%i\n", trackNo, trackLength);
                        for (z=0; z < depth; z++)               //corr
                        {
                                int p = connectivityTable[z][n];
                                if (p < n_part_z[z]) // if it is an existing particle
                                {
                                        //fprintf(fp, "%i %i %f %f %f %f\n", z, connectivityTable[z][n], particles[z][p].x, particles[z][p].y, particles[z][p].orientation.x, particles[z][p].orientation.y);
                                        if (version > 0.0) // old file format (obsolete):
                                        {
                				// diameter*0.8 is subtracted to give the distance between the points of two spherocylinders. Now the found distance (in pixels) is given again.
                                                //fprintf(fp, "%i %i %f %f %f %f %f %f %f %f\n", z, connectivityTable[z][n], particles[z][p].x, particles[z][p].y, particles[z][p].orientation.x, particles[z][p].orientation.y, particles[z][p].length+diameter*0.8, particles[z][p].ar1, particles[z][p].ar2, particles[z][p].ar3);
                                                fprintf(fp, "%i %i %f %f %f %f %f %f %f %f\n", z, connectivityTable[z][n], particles[z][p].x, particles[z][p].y, particles[z][p].orientation.x, particles[z][p].orientation.y, particles[z][p].length+particles[z][p].width*0.8, particles[z][p].ar1, particles[z][p].ar2, particles[z][p].ar3);
                                        }
                                        else if (version < 0.0) // new file format:
                                        {
                                                fprintf(fp, "%i %i %f %f %f %f %f %f %f %f %f %f\n", z, connectivityTable[z][n], particles[z][p].x, particles[z][p].y, particles[z][p].orientation.x, particles[z][p].orientation.y, particles[z][p].length, particles[z][p].ar1, particles[z][p].ar2, particles[z][p].ar3, particles[z][p].width, particles[z][p].accuracy);
						// writing a distribution file with lengths and widths for each trajectory:
                                        }

                                }
                                else
                                {
                                }
                        }
                        trackNo++;
                }
        }

        fclose(fp);
        printf("+++ Done.\n");
}

/* Function: freeMemory
        Free allocated memory. The function doesn't free the pointer itself, only the underlying arrays.

        Parameters:
                connectivityTable - contains connections between particles in consecutive frames
                n_part_z - list of the number of particles for each frame
                particles - contains a list of particle coordinates for each frame
                depth - number of frames
                cellList - cell list used used to speed up the calculations
                nearestNeighbourTable - list of nearest neighbours for each particle in each frame
                maximumNumberofBoxes_x - maximum number of cells in x direction
                maximumNumberofBoxes_y - maximum number of cells in y direction
*/
void freeMemory(int** connectivityTable, int* n_part_z, tBac ** particles, int depth, int **** cellList, int *** nearestNeighbourTable, int maximumNumberofBoxes_x, int maximumNumberofBoxes_y)
{
	printf("*** Freeing memory..\n");

	int z;
	for (z = 0; z < (depth); z++)
	{
		int m=0;
        	for(m = 0; m < n_part_z[z]; m++) // particle m in layer z
	        {
                	free(nearestNeighbourTable[z][m]);
	        }

        	free(nearestNeighbourTable[z]);
	}

        for (z = 0; z < depth; z++)
        {
		int x,y;
                for (x = 0; x < maximumNumberofBoxes_x; x++)
                {
                        for (y = 0; y < maximumNumberofBoxes_y; y++)
                        {
                                free(cellList[z][x][y]);
                        }
                        free(cellList[z][x]);
                }
                free(cellList[z]);
        }

        for (z = 0; z < depth; z++)
                free(connectivityTable[z]);

	printf("+++ Done.\n");
}

/* Function: reserveMemory
        Reserves the memory required for the calculations

        Parameters:
                connectivityTable - contains connections between particles in consecutive frames
                allocatedSoFar - maximum number of trajectories that can be stored in memory 
		n_part_z - list of the number of particles for each frame
                particles - contains a list of particle coordinates for each frame
                depth - number of frames
                cellList - cell list used used to speed up the calculations
		nearestNeighbourTable - list of nearest neighbours for each particle in each frame
		maximumNumberofBoxes_x - maximum number of cells in x direction
		maximumNumberofBoxes_y - maximum number of cells in y direction
*/
void reserveMemory(int** connectivityTable, int allocatedSoFar, int* n_part_z, tBac ** particles, int depth, int **** cellList, int *** nearestNeighbourTable, int maximumNumberofBoxes_x, int maximumNumberofBoxes_y)
{
	// Currently, this strategy is to reserve all the required memory for book-keeping before starting the tracking.

        printf("*** Allocating memory..\n");
	int z,n;
	for (z = 0; z < depth; z++)
        {
                nearestNeighbourTable[z] = (int**) malloc((n_part_z[z])*sizeof(int*));
                for (n = 0; n < n_part_z[z]; n++)
                {
                        nearestNeighbourTable[z][n] = (int*) malloc(1*sizeof(int));
                        nearestNeighbourTable[z][n][0] = 0;
                }
        }

        for (z = 0; z < depth; z++)
        {
		int x,y;
                cellList[z] = (int***) malloc(maximumNumberofBoxes_x*sizeof(int**));
                for (x = 0; x < maximumNumberofBoxes_x; x++)
                {
                        cellList[z][x] = (int**) malloc(maximumNumberofBoxes_y*sizeof(int*));
                        for (y = 0; y < maximumNumberofBoxes_y; y++)
                        {
                                cellList[z][x][y] = NULL;
                        }
                }
        }

	for (z = 0; z < (depth); z++)
        {
                connectivityTable[z] = NULL;
                connectivityTable[z] = (int*) malloc(allocatedSoFar*sizeof(int)); // make sure there is enough space.
                if (connectivityTable[z] == NULL)
                {
			// this happens when memory allocation failed
                        printf("Cannot reserve memory for connectivitytable. Out of memory!!!!\n");
                        exit(666);
                }

                for (n = 0; n < allocatedSoFar; n++) // only fill for z = 0.
                {
                        connectivityTable[z][n] = n_part_z[z]+1; // this means that by default connectivityTable[z][n] is not part of a trajectory
                }
        }

	printf("+++ Done.\n");
}

/* Function: setupConnectivityTable
        Sets up the connectivityTable

        Parameters:
                startFrame - earliest frame where a trajectory can start
                n_part_z - list of the number of particles for each frame
                connectivityTable - contains connections between particles in consecutive frames
		lastIndices - list of last indices
*/
void setupConnectivityTable(int startFrame, int* n_part_z, int** connectivityTable, int* lastIndices)
{
	printf("*** Setting up connectivity-table..\n");
	int n = 0;
        for (n = 0; n < n_part_z[startFrame]; n++) // only fill for z = 0.
        {
                connectivityTable[startFrame][n] = n; // put indexes of all the particles in the first frame in the connectivityTable.
                lastIndices[n] = n;
        }
	printf("+++ Done.\n");
}

/* Function: setupCells
        Sets up the cell list, and attributes particles to each cell

	Parameters:
		n_part_z - list of the number of particles for each frame
		particles - contains a list of particle coordinates for each frame
                depth - number of frames
		cellList - cell list used used to speed up the calculations
		r_c_inv_x - 1/r_c_x, used to calculate the cell that a particle belongs to
		r_c_inv_y - 1/r_c_y, used to calculate the cell that a particle belongs to
*/
void setupCells(int* n_part_z, tBac ** particles, int depth, int **** cellList, double r_c_inv_x, double r_c_inv_y)
{
	printf("*** Setting up cell-list (%i frames)..\n", depth);

	int z = 0;
	int n = 0;

        for (z = 0; z < depth; z++)
        {
		//printf("%i\n", z);
                for (n = 0; n < n_part_z[z]; n++)
                {
                        //printf("%f %f\n", particles[z][n].x, particles[z][n].y);
                        int x_cell = ( (int)ceil(particles[z][n].x * r_c_inv_x) )-1;    // x_cell
                        int y_cell = ( (int)ceil(particles[z][n].y * r_c_inv_y) )-1;    // y_cell                       
                        //printf("%i %i %f %f\n", x_cell, y_cell, particles[z][n].x, particles[z][n].y);
                        if (y_cell < 0 || x_cell < 0)
                        {
                                printf("look for errors in input file!\n");
                                printf("cells: %i %i coordinates: %f %f", x_cell, y_cell, particles[z][n].x, particles[z][n].y);
                                exit(666); // should be impossible!
                        }
                        if (cellList[z][x_cell][y_cell] == NULL)                                        // major bug fixed 09-07-2008
                        {
                                cellList[z][x_cell][y_cell] = (int*) malloc(1*sizeof(int));
                                cellList[z][x_cell][y_cell][0] = 0;
                        }
                        cellList[z][x_cell][y_cell][0]++;
                        cellList[z][x_cell][y_cell] = (int*)realloc(cellList[z][x_cell][y_cell], (cellList[z][x_cell][y_cell][0]+1)*sizeof(int));

                        if (cellList[z][x_cell][y_cell] == NULL)
                        {
                                printf("Memory full!\n");
                                exit(666);
                        }

                        cellList[z][x_cell][y_cell][cellList[z][x_cell][y_cell][0]] = n;
                        particles[z][n].x_cell = x_cell;        // labelling the particle with cellList info
                        particles[z][n].y_cell = y_cell;        // labelling the particle with cellList info
                }
        }

	printf("+++ Done.\n");
}

/* Function: performTracking
        Performs the actual tracking

        Parameters:
		startFrame - earliest frame at which a found trajectory can start
                connectivityTable - contains connections between particles in consecutive frames
		allocatedSoFar - maximum number of trajectories that can be stored in memory 
                n_part_z - list of the number of particles for each frame
                particles - contains a list of particle coordinates for each frame
                depth - number of frames
 		cellList - cell list used used to speed up the calculations
                nearestNeighbourTable - list of nearest neighbours for each particle in each frame
                maximumNumberofBoxes_x - maximum number of cells in x direction
                maximumNumberofBoxes_y - maximum number of cells in y direction
		lastIndices - list of last indices

        Returns:
                endList - number of found trajectories 
*/
int performTracking(int startFrame, int** connectivityTable, int allocatedSoFar, int* n_part_z, tBac ** particles, int depth, int **** cellList, int *** nearestNeighbourTable, int maximumNumberofBoxes_x, int maximumNumberofBoxes_y, int* lastIndices)
{
	printf("*** Starting the tracking!\n"); 
	int endList = n_part_z[startFrame]; // last value in connectivityList

	int m = 0;
	int n = 0;
	int z=startFrame;

	int displayInterval = (depth / 10);
	for (z = startFrame; z < (depth-1); z++) // upto (depth -1) because this is the last frame to relate...
        {
                if (z % displayInterval == 0)
                        printf("--> Frame: %i..\n", z);

                // looking for nearest neighbours in next picture (z+1) and list them
                for(m = 0; m < n_part_z[z]; m++) // particle m in layer z
                {
                        TT = 2*particles[z][m].length;
                        //TT = 10*particles[z][m].length;
                        
			int jbox_x=0;
        	        int jbox_y=0;

			int start_x = particles[z][m].x_cell;
			if (particles[z][m].x_cell > 0)
				start_x = particles[z][m].x_cell-1;
	
			int end_x = particles[z][m].x_cell+1;		
			if (particles[z][m].x_cell < maximumNumberofBoxes_x-1)
				end_x = (particles[z][m].x_cell+2);
			
                        int start_y = particles[z][m].y_cell;
			if (particles[z][m].y_cell > 0)
                                start_y = particles[z][m].y_cell-1;

                        int end_y = particles[z][m].y_cell+1;
                        if (particles[z][m].y_cell < maximumNumberofBoxes_y-1)
                                end_y = particles[z][m].y_cell+2;

                        for(jbox_x = start_x; jbox_x < end_x; jbox_x++)
                        {       // surrounding cells in x direction 
                        	for(jbox_y = start_y; jbox_y < end_y; jbox_y++)
                                {       // surrounding cells in y direction                             
                                        if (cellList[z+1][jbox_x][jbox_y] != NULL)
                                        {
						// if the cell-list is not empty:
                                                for (n = 1; n < (cellList[z+1][jbox_x][jbox_y][0]+1); n++)
                                                {
                                                        int p_n = cellList[z+1][jbox_x][jbox_y][n];
                                                        if ( (distance2(particles[z][m], particles[z+1][p_n]) < (TT*TT) ) ) // check if particle n in layer z+1 is at a distance < r_c from particle m in layer z
                                                                nearestNeighbourTable[z][m] = updateCandidates(nearestNeighbourTable[z][m], particles , z, m, p_n);
                                                }
                                        }
                                }
                        }
                }
                endList = linkList(nearestNeighbourTable[z], connectivityTable, n_part_z, z, particles, endList, depth, lastIndices);
                if (endList > allocatedSoFar )
                {
                        printf("!!! Problem, not enough memory was allocated - exiting program \n");
                        exit(666);
                }
        }
	
	printf("+++ Done.\n"); 
	return endList;
}

/* Function: calculateMaxParticlesInFrame
        Calculates the maximum number of particles in any single frame

        Parameters:
                n_part_z - list of the number of particles for each frame
                depth - number of frames

        Returns:
        	n_part_z_max - maximum number of particles in a single frame
*/
int calculateMaxParticlesInFrame(int depth, int* n_part_z)
{
	int n_part_z_max = 0;
	int z = 0;
        for (z = 0; z < (depth); z++)
                if (n_part_z[z] > n_part_z_max)
                        n_part_z_max = n_part_z[z]; // find the layer with the largest number of particles and put #particles for this layer in n_part_z_max

	return n_part_z_max;
}

/* Function: computePossibleTracks
        Computes trajectories from lists of particle coordinates for each frame

        Parameters:
                particles - contains a list of particle coordinates for each frame
                n_part_z - list of the number of particles for each frame
                depth - number of frames
                outputTracks - output file to write trajectories
                dim_x - frame width in pixels
                dim_y - frame height in pixels
		version - version of the file format
*/
void computePossibleTracks(tBac ** particles, int* n_part_z, int depth, char* outputTracks, float dim_x, float dim_y, float version, int startFrame)
{
	WARNINGS = true;
	printf("*** Setting up initial variables..\n");

	//int startFrame = 100;

	double frameWidth = dim_x;
	double frameHeight = dim_y;

	printf("--> Frame size: %f by %f\n", frameWidth, frameHeight);
	printf("--> Depth: %i frames \n", depth);
	
	int *** nearestNeighbourTable = (int***) malloc((depth)*sizeof(int**));
	int ** connectivityTable = (int**) malloc(depth*sizeof(int*));

	int **** cellList = (int****) malloc((depth)*sizeof(int***));	// array of 2D cellLists

	// Start define parameters for cell-list:
		
	double sizeOfCellInCellList = 50.0;
	double r_c_y = 2*sizeOfCellInCellList; // critical size for cell list, fast particles need large cell lists, else they are not detected.
	double r_c_x = 2*sizeOfCellInCellList;
	double r_c_inv_y = 1/r_c_y;
	double r_c_inv_x = 1/r_c_x;
	
	int maximumNumberofBoxes_x = (int)ceil((double)frameWidth/r_c_x);	
	int maximumNumberofBoxes_y = (int)ceil((double)frameHeight/r_c_y);	
	
	printf("--> Using 2D cells with dimensions %f x %f.. \n", r_c_x, r_c_y); 

	// End define parameters for cell-list

	int n_part_z_max = calculateMaxParticlesInFrame(depth, n_part_z);
	printf("--> Max number of particles in frame: %i\n", n_part_z_max);
	int max_dim = depth*n_part_z_max; // the maximum number of tracks possible
	printf("--> Max dimension: %i\n", max_dim);	
	
	int allocatedSoFar = max_dim;

	if (max_dim > 40000000/200) // so it doesn't use too much memory
		allocatedSoFar = 40000000/200;

	printf("--> Memory to be allocated: %i (%i x %i)\n", allocatedSoFar, depth, n_part_z_max);

	int* lastIndices = (int*) malloc(n_part_z_max*sizeof(int));

	int nn = 0;
	for (nn = 0; nn < n_part_z_max; nn++)
		lastIndices[nn] = 0;

	printf("+++ Done.\n");

	reserveMemory(connectivityTable, allocatedSoFar, n_part_z, particles, depth, cellList, nearestNeighbourTable, maximumNumberofBoxes_x, maximumNumberofBoxes_y);

	setupCells(n_part_z, particles, depth, cellList, r_c_inv_x, r_c_inv_y);

	setupConnectivityTable(startFrame, n_part_z, connectivityTable, lastIndices);

	int endList = performTracking(startFrame, connectivityTable, allocatedSoFar, n_part_z, particles, depth, cellList, nearestNeighbourTable, maximumNumberofBoxes_x, maximumNumberofBoxes_y, lastIndices);

	writeTracksToFiles(outputTracks, connectivityTable, n_part_z, particles, startFrame, endList, depth, version);

	freeMemory(connectivityTable, n_part_z, particles, depth, cellList, nearestNeighbourTable, maximumNumberofBoxes_x, maximumNumberofBoxes_y);

	free(cellList);
	free(connectivityTable);
	free(nearestNeighbourTable);
	free(lastIndices);
	free(n_part_z);

	printf("Tracking complete..\n");
}
