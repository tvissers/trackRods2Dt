/*
    This file is part of trackRods2Dt.

    trackRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    trackRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with trackRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

// type definitions

typedef struct 
{
  float x;
  float y;
  //float z;
} dir;

typedef struct 
{
  float x;
  float y;
  //float z;
  int x_cell;
  int y_cell;
  int ID;                // for test runs, ID tag given by simulation or generative program.
  dir orientation;
  float length;
  float width;
  dir lastMovement;
  float ar1;
  float ar2;
  float ar3;
  float accuracy;
} tBac;


