------------------------------------------------------------------------------
About: License

    This file is part of trackRods2Dt.

    trackRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    trackRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with trackRods2Dt.  If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
About: This program

This program calculates and outputs trajectories from positions of rod-shaped objects in consecutive frames.

Please cite the following papers if you use this code for a scientific publication:

T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018.
https://doi.org/10.1126/sciadv.aao1170

T. Vissers et al., PloS One., 14, 6, e0217823, 2019.
https://doi.org/10.1371/journal.pone.0217823

--------------------------------------------------------------------------------

About: repository

At the time of publication, a repository for this code is available at: https://git.ecdf.ed.ac.uk/tvissers/trackRods2Dt
Updated versions may appear there.

--------------------------------------------------------------------------------

About: Running the example

Type 'make' to compile the code and create the executable.

To run the testcase a trackRods2Dt.in should be provided with this program. This file
contains the setting used by the tracking algorithm and should look as follows:

--- text -----------------------------------------------------------------------
INPUTFILE ../testcase/input/positions.dat
OUTPUTFILE ../testcase/output/tracks.dat
DEBUG 1
STARTFRAME 100
--------------------------------------------------------------------------------

About: Input files

* positions.dat file containing positions and orientations of rod-shaped objects, obtained with findRods2Dt.

--------------------------------------------------------------------------------

About: Output files

* tracks.dat file containing trajectories.

--------------------------------------------------------------------------------

About: tracks.dat - file format

This file contains trajectories constructed from the positions in consecutive frames originally obtained with findRods2Dt.

--- text -----------------------------------------------------------------------
&NUMBEROFTRAJECTORIES &NUMBEROFFRAMES VERSION_ID
&TRAJECTORYNO &TRAJECTORYDURATION
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
..
..
..
&TRAJECTORYNO &TRAJECTORYDURATION
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
..
..
--------------------------------------------------------------------------------

Legend:

--- text -----------------------------------------------------------------------
NUMBEROFTRAJECTORIES 	: 	total number of trajectories
NUMBEROFFRAMES 		: 	total number of frames
VERSION_ID		: 	negative number is new (default) format, positive number is old format
--------------------------------------------------------------------------------

For each trajectory is written:

--- text -----------------------------------------------------------------------
TRAJECTORYNO 		:	ID of the trajectory, starts at 0
TRAJECTORYDURATION	:	Duration (in frames) of the trajectory
--------------------------------------------------------------------------------

For each frame in the trajectory is given:

--- text -----------------------------------------------------------------------
FRAMENO		:	frameno since start of the video
PARTICLEID	:	original ID number of rod-shaped object in the frame
X_COM		:	x position in pixels 
Y_COM		:	y position in pixels
X_OR		:	x-component of vector (not normalised) pointing from COM to POLE
Y_OR		:	y-component of vector (not normalised) pointing from COM to POLE
LENGTH		:	length (in pixels)
FITPAR1		:	aspect ratio lengthfromfit /widthfromfit obtained with findRods2Dt
FITPAR2		:	(lengthfromfit-widhtfromfit)/widthfromfit obtained with findRods2Dt
FITPAR3		:	aspect ratio lengthfromfit / estimated width from input file obtained with findRods2Dt
WIDTH		:	width (in pixels)
FITPAR4		:	sqrt(largesteigenvalue/smallesteigenvalue) obtained with findRods2Dt
--------------------------------------------------------------------------------
