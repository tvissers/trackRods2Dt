/*
    This file is part of trackRods2Dt.

    trackRods2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    trackRods2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with trackRods2Dt.  If not, see <http://www.gnu.org/licenses/>.
*/

/* About: trackRods2Dt

This program is designed to track E. coli bacteria for a time-series of 
2D images from a source file containing positions for each frame. 
Written by Teun Vissers.

If you use this code for a scientific publication:
	Please cite [T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018].

Published under the GPLv3 license.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <glob.h>
#include "trackingRoutines.c"

/* Variables: Global variables indicated with CAPITAL LETTERS
  INPUTFILE[1024]  - The file containing the positions and orientations for all frames
  OUTPUTFILE[1024] - File to write the found trajectories
  DEBUG            - Sets the amount of checks and DEBUG output that is generated.
                    0 is minimal (no output), higher numbers will generate more output.
  STARTFRAME	   - Frame to start tracking. Using automated stages, it can happen that the 
		    first frames are a blur or still moving a bit. Skipping the first frames 
                    is a way to deal with that. Default set to 100.
*/

char INPUTFILE[1000]="";
char OUTPUTFILE[1000]="";
int DEBUG = 10;
int STARTFRAME = 100;
glob_t globbuf;

#include "namelist.h"

NameList nameList[] = {
  NameC (INPUTFILE),         // Positions file used as input
  NameC (OUTPUTFILE),        // Trajectory file used as output
  NameI (DEBUG),             // Outputs extra information if set to 1
  NameI (STARTFRAME),             // Outputs extra information if set to 1
};

#include "namelist.c"

void mygetline(char* str,FILE *f)
{
  int comment=1;
  while(comment)
  {
    if (!fgets(str,256,f)) return;
    if(str[0]!='#') comment=0;
  }
}

/* Function: read2Dt
	Reads file with coordinates and orientations, other fit parameters

        Parameters:
                INPUTFILEs - first element is the input file containing particle coordinates and orientations
*/
void read2Dt(char** inputfiles)
{
	  int n_frames=0;

	  FILE* file = fopen(inputfiles[0], "r");
	
	  char str[256];
	  mygetline(str,file);
	  sscanf(str,"&%i", &n_frames); // reads the number of frames

	  if (STARTFRAME > n_frames)
	  {
		printf("Startframe (%i) can not be higher than the number of frames (%i), exiting..\n", STARTFRAME, n_frames);
		exit(555);
	  }
 
	  tBac ** level_n = (tBac**) malloc(n_frames*sizeof(tBac*) );
          
	  int * n_part_n = (int*) malloc(n_frames*sizeof(int) );

	  int n_part=0;
	  int ID = 0;
	  float dim_x;
	  float dim_y;
	  float thisversion=0.0;
	  float version=0.0;
	  float frameWidth =0.0;
  	  float frameHeight =0.0;
          int frame;

	  int n;
          for(n=0; n < n_frames; n++)
          {   	
		mygetline(str,file);
	 	sscanf(str,"&%i &%i", &frame, &n_part); // frame number, number of particles in this frame
		mygetline(str,file);
	 	sscanf(str,"%e %e %e", &dim_x, &dim_y, &thisversion); // dimensions in the x- and y-direction, as well as the diameter.
		frameWidth=dim_x;		
		frameHeight=dim_y;	
		version=thisversion;

		float temp;
		float temp2;

		level_n[n] = (tBac*) malloc((n_part)*sizeof(tBac));
		n_part_n[n] = n_part;	
                
		int i;
          	for(i=0; i < n_part; i++)
	        {
			  tBac thisBac;
			  mygetline(str,file);

			  // Note: this uses the file format of the positions.dat output file containing:
			  // x-coordinate centre of mass, y-coordinate centre of mass, 0.0, x pole1. y pole1, 0.0, fit parameter1, fit parameter2, fit parameter3, width, length, accuracy
	 		  sscanf(str,"%e %e %e %e %e %e %e %e %e %e %e %e", &thisBac.x, &thisBac.y, &temp, &thisBac.orientation.x, &thisBac.orientation.y, &temp2, &thisBac.ar1, &thisBac.ar2, &thisBac.ar3, &thisBac.width, &thisBac.length, &thisBac.accuracy);
			  
			  level_n[n][i].x = thisBac.x; // x-coordinate for centre of rod
			  level_n[n][i].y = thisBac.y; // y-coordinate for centre of rod

			  level_n[n][i].orientation.x = thisBac.orientation.x-thisBac.x; // from centre to one pole of rod
			  level_n[n][i].orientation.y = thisBac.orientation.y-thisBac.y; // from centre to one pole of rod

			  level_n[n][i].lastMovement.x = 0.0; // unknown at this moment, so set to 0
			  level_n[n][i].lastMovement.y = 0.0; // unknown at this moment, so set to 0
			  
			  level_n[n][i].ar1 = thisBac.ar1; // a fit parameter
			  level_n[n][i].ar2 = thisBac.ar2; // a fit parameter
			  level_n[n][i].ar3 = thisBac.ar3; // a fit parameter

			  level_n[n][i].width = thisBac.width; // width of rod
			  level_n[n][i].length = thisBac.length; // length of rod
			  level_n[n][i].accuracy = thisBac.accuracy; // a fit parameter

			  ID++;
	         }
          }
	  
	  fclose(file);

          printf("Connecting particles in 2D in time NOW!\n");
	  printf("------- ----- ---------\n");

	  // currently, the code assumes the version in the last frame is the same as for the other frames.	
          computePossibleTracks(level_n, n_part_n, n_frames, OUTPUTFILE, frameWidth, frameHeight, version, STARTFRAME); // connects the particles
}

/* Function: main
	Main routine

        Parameters:
		number of arguments
		list of arguments
*/
int main(int argc, char *argv[])
{
  	  //Read parameter file
	  GetNameList(argc,argv);
 	  PrintNameList (stdout);

	  //tBox *box;

	  //Read image file
	  if(DEBUG) printf("Reading %s\n",INPUTFILE);

	  if(glob(INPUTFILE,GLOB_TILDE,NULL,&globbuf) != 0) printf("WARNING: Failed reading %s\n",INPUTFILE);
	  
	  if(globbuf.gl_pathc < 1) 
	  {
		printf("!!! No files found\n"); 
		exit(666);
	  }
	  else if (globbuf.gl_pathc == 1)
	  {
		printf("------- start ---------\n");
		read2Dt(globbuf.gl_pathv);
	  	printf("------- end ---------\n");
	  }

          return 0;
}
